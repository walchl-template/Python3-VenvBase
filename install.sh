VENV=.venv

find_python() {
	max_v=0

	for p in 3.9 3.8 3.7 3 ; do
		v=$(python$p -V | cut -d'.' -f2)"0"

		if [ $max_v -lt $v ] ; then
			max_v=$v
			best_python="python"$p
		fi
	done
}

if [ -e $1 ]; then
	find_python
else
	best_python=$1
fi

echo "Use "$best_python

VENV=$VENV"_"$best_python

$best_python -m venv $VENV
source $VENV/bin/activate

pip install -U pip
pip install -U wheel
pip install -r requirements.txt
